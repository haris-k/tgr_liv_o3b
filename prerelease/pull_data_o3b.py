#! /usr/bin/env python

# -*- coding: utf-8 -*-
#
#       Copyright 2020
#       Maximiliano Isi <max.isi@ligo.org>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import numpy as np
from glob import glob
import os
import re

from utils import Event
from livutils import LambdaPairs, default_alpha_string
os.environ['H5PY_DEFAULT_READONLY'] = '1'

data_path = '/home/tgr.o3/o3b-tgr/results/liv/o3b_posteriors/'
cache_path = 'gwtc3_data/{label}/liv_{label}_{param}.dat.gz'
alphas = np.delete(np.arange(0, 4.5, 0.5), 4)
nsamp = 10000

# get events from directory
event_dirs = glob(os.path.join(data_path, 'S*'))
events = [os.path.basename(ed) for ed in event_dirs]
path = os.path.join(data_path, '{label}/liv_{label}_A{sign}_alpha{alpha}.h5')

for alpha in alphas:
    print('#'*80 + '\nPROCESSING ALPHA {}\n'.format(alpha) + '#'*80)
    # load lambda_alpha samples for all events found
    results = LambdaPairs.from_path(path, labels=events, alpha=alpha,
                                    force_pesummary=True)
    print('-'*80+'\nAlpha {} results loaded for {} events.\n'.format(alpha,
          len(results)) + '-'*80)
    if alpha == 0:
        # get massive graviton posteriors
        # first, reweight A>0 lambda samples to have a prior uniform in mass
        results_mg = results.results[1].reweight('mass', nsamples=nsamp)
        # then, convert wavelength into an energy scale (mc^2) and store
        samples_mg = results_mg.energies
        for gid, samples in zip(results_mg.labels, samples_mg):
            p = cache_path.format(label=Event(gid).cid, param='mg')
            os.makedirs(os.path.dirname(p), exist_ok=True)
            np.savetxt(p, samples)
            print("Saved: %r" % p)
    print('-'*80)
    # get amplitude posteriors
    # in one step, reweight both A>0 and A<0 samples to have a prior uniform in
    # A and, convert to amplitudes, joining A>0 and A<0 samples, and store
    samples_amp = results.draw_all_twosided_amplitudes(nsamples=nsamp,
                                                       kde_kws={'reweight':'A'})
    param = 'alpha' + default_alpha_string(alpha)
    for gid, samples in zip(results.labels, samples_amp):
        p = cache_path.format(label=Event(gid).cid, param=param)
        os.makedirs(os.path.dirname(p), exist_ok=True)
        np.savetxt(p, samples)
        print("Saved: %r" % p)
    # additionally, get PDFs directly
    amp_pdfs = results.get_all_twosided_amplitude_pdfs(nbins=5000,
                                                       kde_kws={'reweight':'A'})
    for gid, samples in zip(results.labels, amp_pdfs):
        p = cache_path.format(label=Event(gid).cid, param=param+'_pdf')
        os.makedirs(os.path.dirname(p), exist_ok=True)
        np.savetxt(p, samples)
        print("Saved: %r" % p)
