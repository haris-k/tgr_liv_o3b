# -*- coding: utf-8 -*-
#
#       Copyright 2020
#       Maximiliano Isi <max.isi@ligo.org>
#       Nathan, Stas, ..... [add me]
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import sys
import os
from glob import glob

from numpy import *
import numpy as np
from scipy import stats
from scipy.interpolate import interp1d
import lal
from lalinference.bayespputils import calculate_redshift, lambda_a
from utils import Bounded_1d_kde

np.random.seed(1234)
    
# ############################################################################
# FUNCTIONS
# ############################################################################

PEV = 1E-12
HPLANCK = 4.135667662E-15

# ----------------------------------------------------------------------------
# LIV-specific utilities

def pev_factor(alpha):
  """ Compute PeV scaling factor.

  Arguments
  ---------
  alpha: float
      dispersion index

  Returns
  -------
  pev_factor: float
      PEV^(2-alpha)
  """
  return PEV**(2.-alpha)

def energy_scale(lambda_A):
    """ Calculate mass/energy scale in eV.
    m c^2 = h c / \lambda_A
    Valid for alpha != 2

    Arguments
    ---------
    lambda_A: float
        wavelength of GR correction

    Returns
    -------
    energy_scale: float
        energy in electronvolts
    """
    return HPLANCK*lal.C_SI/lambda_A

def A_LIV(lambda_A, alpha):
    """ Get amplitude from wavelength.

    Calculate \mathbb{A}*c^{2-alpha} in (eV)^{2-a} as: 
    A = (m_A*c)^{2-alpha} = (h/lambda_A)^{2-alpha}
    Valid for alpha != 2

    Arguments
    ---------
    lambda_A: float
        wavelength of GR correction
    alpha: float
        dispersion index

    Returns
    -------
    amplitude: float
        magnitude of GR correction corresponding to wavelength.
    """
    if alpha == 2:
        raise ValueError("cannot compute amplitude for alpha = 2")
    return (HPLANCK*lal.C_SI/lambda_A)**(2-alpha)

def lambda_A_of_eff(leffedata, alpha, z=None, dist=None, cosmology=None):
    """ Compute the wavelength of the GR correction from an effective
    wavelength by using information about the distance and redshift.

    Must provide cosmology information by passing any two of:
      z, dist, cosmology
    If all of these are passed `cosmology` is ignored.

    Arguments
    ---------
    leffdata: float, array
        effective-wavelength samples
    alpha: float
        dispersion index
    z: float, array
        redshift samples
    dist: float, array
        luminosity distance samples
    cosmology: LALCosmology
        cosmology object

    Returns
    -------
    lamdba_a: float, array
        wavelength samples
    """
    if dist is None and z is not None:
        dist = vectorize(lal.LuminosityDistance, excluded=[0])(cosmology, z)
    elif dist is not None and z is None:
        c = cosmology
        z = vectorize(calculate_redshift)(dist, c.h, c.om, c.ol, c.w0)
    return lambda_a(z, alpha, leffedata, dist)

def get_cosmology(cosmology):
    if isinstance(cosmology, lal.CosmologicalParameters):
        return cosmology
    # Set the cosmology parameters
    if cosmology == "2015":
        # same Planck 2015 values as in the bayespputils redshift
        # calculation; i.e., the TT+lowP+lensing+ext values from Table 4 of
        # Ade et al., A&A 594, A13 (2016), arXiv:1502.01589
        cosmo_params = (0.6790, 0.3065, 0.6935, -1.0, 0.0, 0.0)
    elif cosmology == "2018":
        # Planck 2018 values used in the O2 catalogue paper for comparison;
        # i.e., the TT,TE,EE+lowE+lensing+BAO values from Table 2 of Aghanim
        # et al., arXiv:1807.06209
        cosmo_params = (0.6766, 0.3111, 0.6889, -1.0, 0.0, 0.0)
    else:
        raise ValueError("cosmology has to be either 2015 or 2018")
    return lal.CreateCosmologicalParameters(*cosmo_params)

# ----------------------------------------------------------------------------
# Analytic prior functions

def jacobian_mass(loglA, loglAmin, loglAmax, alpha=None):
    """ Prior uniform in mass scale.

    Arguments
    ---------
    loglA: float, array
        log10-wavelength samples.
    loglAmin: float
        minimum log10-wavelength allowed.
    loglAmax: float
        maximum log10-wavelength allowed.
    alpha: any
        dummy kwarg (see `jacobian_A`)

    Returns
    -------
    prior: float, array
        normalize prior PDF corresponing to loglA samples.
    """
    return log(10)*(pow(10.0, loglAmax) - pow(10.0, loglAmin))/\
           (pow(10.0, loglA + loglAmax + loglAmin))

def jacobian_A(loglA, loglAmin, loglAmax, alpha=None):
    """ Prior uniform in amplitude.

    Arguments
    ---------
    loglA: float, array
        log10-wavelength samples.
    loglAmin: float
        minimum log10-wavelength allowed.
    loglAmax: float
        maximum log10-wavelength allowed.
    alpha: float
        dispersion index.

    Returns
    -------
    prior: float, array
        normalize prior PDF corresponing to loglA samples.
    """
    return log(10)*(alpha - 2.0)*pow(10.0, loglA*(alpha - 2.0))/\
           (pow(10.0, loglAmax*(alpha - 2.0)) -
            pow(10.0, loglAmin*(alpha-2.0)))

jacobian = {'MASS': jacobian_mass, 'A': jacobian_A}

# ----------------------------------------------------------------------------
# Naming

def guess_alpha(path):
    """ Extract dispersion index from filepath.

    Arguments
    ---------
    path: str
        file path

    Returns
    -------
    alpha: float
        best guess from alpha based on standard pattern.
    """
    filename = os.path.splitext(os.path.basename(path))[0]
    return float(filename.split('alpha')[1].replace('p','.'))

def default_alpha_string(alpha):
    """ Produce string to label files for a given alpha.

    e.g. alpha = 0.5  -->  '0p5'

    Arguments
    ---------
    alpha: float
        dispersion index

    Returns
    -------
    string: str
        standard string for alpha.
    """
    return str(alpha).replace('.', 'p').replace('p0', '')

# ----------------------------------------------------------------------------
# Stats utils

def normalize(pdf, x):
    """Normalize a 1-dim posterior function on a given grid"""
    return pdf/trapz(pdf,x)

def draw_samples(pdf, x_min, x_max, nsamp=1000, emcee=False, **kwargs):
    """ Make `nsamp` draws from PDF using rejection sampling, or a quick MCMC.

    Arguments
    ---------
    pdf: function
        probability density function for quantity x.
    x_min: float
        minimum value of x.
    x_max: float
        maximum value of x.
    nsamp: int
        number of samples to draw through rejection sampling..
    emcee: bool
        use emcee instead of rejection sampling.
    """
    if emcee:
        # use emcee to sample PDF
        import emcee
        nwalkers = kwargs.pop('nwalkers', 6)
        nsteps = kwargs.pop('nsteps', 10000)
        def lnlike(x):
            if x < x_min or x > x_max:
                return -np.inf
            else:
                return np.log(pdf(x))
        sampler = emcee.EnsembleSampler(nwalkers, 1, lnlike)
        p0 = np.random.uniform(x_min, x_max, (nwalkers, 1))
        sampler.run_mcmc(p0, nsteps)
        tau = sampler.get_autocorr_time()
        burnin = int(kwargs.get('nburnin', 2)*np.max(tau))
        thin = int(0.5*np.min(tau))
        samples = sampler.chain[:, burnin:-1:thin, :].reshape(-1,1)[:,0]
    else:
        p_max = kwargs.pop('p_max', None)
        if p_max is None:
            # guess maximum of PDF
            x = np.linspace(x_min, x_max, 1000)  
            p_max = max(pdf(x))
        # rejection sampling
        sample_list = []
        for i in range(nsamp):
            sample = np.random.uniform(x_min, x_max) # 10*x_max
            while pdf(sample) < np.random.uniform(0, p_max):
                sample = np.random.uniform(x_min, x_max)
            sample_list.append(sample)
        samples = np.array(sample_list)
    return samples


# ############################################################################
# CLASSES
# ############################################################################

class LambdaSamples(object):
    _keys = ["log10lambda_a", "lambda_a", "lambda_eff", "log10lambda_eff"]
    _dist_keys = ['luminosity_distance', 'logdistance', 'distance', 'dist']

    def __init__(self, data=None, log_data=None, alpha=None, log_evidence=None,
                 prior='default', path=None, label=None):
        """ Container for non-effective lambda_A samples.

        Can be created from samples on the quantity and/or its base-10 log.

        Arguments
        ---------
        data: array
            lambda_A samples.
        log_data: array
            log10(lambda_A) samples.
        alpha: float
            dispersion index.
        log_evidence: float
            logZ associated with run (opt.)
        prior: str
            type of prior used in run (def. 'DEFAULT')
        path: str
            data origin (opt.)
        label: str
            run identifier (opt.)
        """
        # basic attributes
        self.alpha = alpha
        self.prior = prior.upper()
        self.log_evidence = log_evidence
        self.path = path
        self.label = label
        # data containers
        self._data = data
        self._log_data = log_data
        for k in ['_data', '_log_data']:
            v = getattr(self, k)
            if v is not None:
                setattr(self, k, np.array(v).flatten())
                self.shape = getattr(self, k).shape
        # initialize cached attributes
        self._amplitudes = None
        self._energies = None
        self._weights_dict = {}

    def __len__(self):
        if self._data is not None:
            return len(self._data)
        elif self._log_data is not None:
            return len(self._log_data)
        else:
            return 0

    # -------------------------------------------------------------------------
    # PROPERTIES

    @property
    def attributes(self):
        return {k: getattr(self, k) for k in ['alpha', 'prior', 'log_evidence',
                                              'path', 'label']}

    @property
    def data(self):
        if self._data is None:
            self._data = pow(10, self._log_data)
        return self._data

    @property
    def log_data(self):
        if self._log_data is None:
            self._log_data = np.log10(self._data)
        return self._log_data

    def get_energies(self):
        """ Obtain energy scale from lambda_A samples.

        Fot alpha = 0 this corresponds to the graviton mass:

        > Eg = mg * c**2 = h * c / lambda0
        """
        return energy_scale(self.data)

    @property
    def energies(self):
        if self._energies is None:
            self._energies = self.get_energies()
        return self._energies

    @property
    def amplitudes(self):
        """ Obtain A samples from lambda_A samples.
        """
        if self._amplitudes is None:
            self._amplitudes = A_LIV(self.data, self.alpha)
        return self._amplitudes

    # def compute_weights(self, kind):
    #     """ Get weights neeed to reweight samples by a given prior.

    #     Arguments
    #     ---------
    #     kind: str
    #         prior to reweight by: 'mass', 'A'

    #     Returns
    #     -------
    #     weights: array
    #         array of weights

    #     """
    #     kind = kind.upper()
    #     if kind == 'MASS':
    #         logweights = 1.0/lambdaA.data
    #         weights = lambdaA.data**(-2)
    #     elif kind == 'A':
    #         logweights = lambdaA.data**(self.alpha-2)
    #         weights = lambdaA.data**(self.alpha-3)
    #     else:
    #         weights = np.ones(self.shape)
    #         logweights = np.ones(self.shape)
    #     return weights, logweights

    # def get_weights(self, kind):
    #     kind = kind.upper()
    #     if kind not in self._weights_dict:
    #         self._weights_dict = self.compute_weights(kind)
    #     return self._weights_dict[kind]

    # -------------------------------------------------------------------------
    # METHODS

    @classmethod
    def from_samples(cls, samples, keys=None, alpha=None, **kwargs):
        """ Create object from pre-loaded samples.

        The samples must include lambda_A info: the quantity or the log of
        lambda_A or lambda_eff. The conversion to lambda_A is done internally.

        Arguments
        ---------
        samples: ndarray, dict
            object containing samples by parameter name,
            e.g. p_samples = samples[p_key] must be an array of samples

        keys: list
            list of column names (def. try `samples.keys()`) 

        alpha: float
            dispersion index
        """
        if keys is None:
            keys = samples.keys()
        data, log_data = None, None
        # see if a specific lambda key was requested
        lk = kwargs.pop('lambda_key', None)
        if lk is None:
            lambda_keys = cls._keys
        else:
            lambda_keys = [lk]
        # loop over possible lambda_A keys
        for key in lambda_keys:
            if key in keys:
                if "log10" in key:
                    log_data = samples[key]
                else:
                    data = samples[key]
                if 'eff' in key:
                    # get luminosity distance from samples
                    dist = None
                    for distance_key in cls._dist_keys:
                        if distance_key in keys:
                            dist = samples[distance_key]
                            if 'log' in distance_key:
                                dist = np.exp(dist)
                            break
                    if dist is None:
                        raise ValueError("no distance samples found.")
                    # obtain lambda_A from effective_lambda_A
                    out = cls.from_effective(data=data, log_data=log_data,
                                             alpha=alpha, dist=dist, **kwargs)
                else:
                    # store lambda_A data directly
                    kwargs.pop('cosmology')
                    out = cls(data, log_data, alpha, **kwargs)
                return out
        return None

    @classmethod
    def from_effective(cls, data=None, log_data=None, alpha=None, z=None,
                       dist=None, cosmology=None, **kwargs):
        """ Obtain lambda_A samples from effective_lambda_A samples.

        Arguments
        ---------
        data: array
            effective lambda_A samples.
        log_data: array
            log(effective_lambda_A) samples.
        z: array
            redshift samples.
        alpha: float
            dispersion index.
        cosmology: LALCosmology
            cosmology used for the conversion.
        """
        if data is None:
            data = pow(10, log_data)
        if all([v is None for v in [z, dist]]) and cosmology is None:
            raise ValueError("please provide cosmology.")
        newdata = lambda_A_of_eff(data, alpha, z=z, dist=dist,
                                  cosmology=cosmology)
        return cls(data=newdata, alpha=alpha, **kwargs)

    # -------------------------------------------------------------------------
    # UTILS

    def reweight(self, prior, nbins=1000, kde_kws=None, **kwargs):
        """ Reweight samples according to new prior.

        Arguments
        ---------
        prior: str
            target prior type indicating uniformly-sampled quantity
            (e.g. 'mass', or 'A').

        nbins: int
            number of bins to interpolate KDE.

        Returns
        -------
        lambdaliv: LambdaSamples
            new object with reweighted samples.
        """
        prior = prior.upper()
        if self.prior == prior:
            return self
        else:
            prior_pdf = jacobian[prior]
        if kde_kws is None:
            kde_kws = {}
        # get KDE of original log_lambda_A samples, treating the edges 
        # carefully with `Bounded_1d_kde`
        old_post_samples = self.log_data
        xlow = min(old_post_samples)
        xhigh = max(old_post_samples)
        old_post_kde = Bounded_1d_kde(old_post_samples, xlow=xlow, xhigh=xhigh,
                                      **kde_kws)
        # could call the KDE directly, but interp1d seems *much* faster!
        x = linspace(xlow, xhigh, nbins)
        old_post_pdf = interp1d(x, old_post_kde(x))
        # create reweighted PDF function
        def new_post_pdf(x):
            return old_post_pdf(x) * prior_pdf(x, xlow, xhigh, self.alpha)
        # draw new samples from reweighted KDE
        kwargs['nsamp'] = kwargs.get('nsamp', len(old_post_samples))
        new_post_samples = draw_samples(new_post_pdf, xlow, xhigh, **kwargs)
        attr = self.attributes
        attr['prior'] = prior
        return LambdaSamples(log_data=new_post_samples, **attr)


class LambdaSet(object):
    def __init__(self, lambdas=None, cosmology='2015'):
        """ Container for multiple sets of LIV lambda_A samples.

        Arguments
        ---------
        prior: str
          use prior uniform in {mass, A} (default is uniform in parameter used)
        cosmology: str,int
          use Planck 2015 or 2018 cosmology parameters (def. '2015')
        """
        self.cosmology = get_cosmology(cosmology)
        self.lambdas = [] if lambdas is None else lambdas

    def __len__(self):
        return len(self.lambdas)

    @property
    def labels(self):
        return [l.label for l in self.lambdas]

    @property
    def paths(self):
        return [l.path for l in self.lambdas]

    @property
    def alphas(self):
        return np.array([l.alpha for l in self.lambdas])

    @property
    def energies(self):
        return [l.energies for l in self.lambdas]

    # -------------------------------------------------------------------------
    # ACCESS UTILS

    def add_result(self, lambdaliv):
        """ Add set of lambda_A samples in container object.

        Arguments
        ---------
        lambdaliv: LambdaSamples
            object containing lambda_A samples
        """
        if lambdaliv.label is None:
            lambdaliv.label = str(len(self.lambdas)+1)
        self.lambdas.append(lambdaliv)

    def add_result_data(self, **kwargs):
        """ Add set of lambda_A samples in array.

        Creates new LambdaSamples object (see docstring for info).
        """
        self.add_result(LambdaSamples(**kwargs))

    def remove_lambda(self, label):
        if label in self.labels:
            idx = self.labels.index(label)
        else:
            idx = label
        del self.lambdas[idx]

    def get_lambda(self, label):
        if label in self.labels:
            idx = self.labels.index(label)
        else:
            idx = label
        return self.lambdas[idx]

    def get_all_samples(self, key):
        key = key.lower()
        if key == 'lambda':
            k = 'data'
        elif key == 'loglambda':
            k = 'log_data'
        else:
            k = key
        return [getattr(lambdaliv, k) for lambdaliv in self.lambdas]

    # -------------------------------------------------------------------------
    # IO UTILS

    @classmethod
    def from_paths(cls, paths, labels=None, alphas=None, *args, **kwargs):
        """ Load liv results from paths to posterior files.

        Arguments
        ---------
        paths: lst
            list of paths.
        labels: lst
            list of labels identifying each run.
        alphas: array, float
            values of dispersion index (if scalar, same for all files).
        """
        # parse path
        if isinstance(paths, str):
            paths = [paths]
        # parse alphas
        try:
            alphas[0]
        except (TypeError,IndexError):
            alphas = [alphas]*len(paths)
        # parse labels
        if labels is None:
            labels = [str(i) for i in range(len(paths))]
        # parse pesummary options
        pelabel = kwargs.pop('pelabel', None)
        if pelabel is None or isinstance(pelabel, str):
            pelabel = [pelabel]*len(paths)
        # create container object
        fp = kwargs.pop('force_pesummary', False) 
        lk = kwargs.pop('lambda_key', None)
        results = cls(*args, **kwargs)
        for i, (label, p) in enumerate(zip(labels, paths)):
            if os.path.exists(p):
                try:
                    results.read_file(p, alphas[i], label=label,
                                      pelabel=pelabel[i], force_pesummary=fp,
                                      lambda_key=lk)
                except Exception as e:
                    print("WARNING: %r file not found in %r" % (label, p))
                    print(e)
            else:
                print("WARNING: %r file not found in %r" % (label, p))
        return results
    
    def read_file(self, path, alpha=None, label=None, pelabel=None, 
                  force_pesummary=False, **kwargs):
        """Load results from LALInference output.

        Arguments
        ---------
        path: str
            path to LALInference or pesummary file.
        label: str
            run name (e.g. event ID)
        pesummary: bool
            whether to load a pesummary file.
        pelabel: str
            run label in pesummary file (def. first label).
        """
        if alpha is None:
            try:
                alpha = self.alpha
            except AttributeError:
                pass
        if alpha is None:
            alpha = guess_alpha(path)
        ext = os.path.splitext(path)[1].strip('.') 
        if ext in  ["hdf5", "h5", "hdf"]:
            from pesummary.gw.file.read import read, is_pesummary_hdf5_file
            if is_pesummary_hdf5_file(path) or force_pesummary:
                f = read(path)
                pelabel = pelabel or f.labels[0]
                data = f.samples_dict[pelabel]
                keys = data.keys()
                i = f.labels.index(pelabel)
                samp = f.extra_kwargs[i]['sampler']
                ln_key = 'log' if 'log_evidence' in samp else 'ln'
                log_evidence = samp.get('%s_evidence' % ln_key)
            else:
                import h5py
                with h5py.File(path, 'r') as f:
                    if 'lalinference_nest' in f['lalinference'].keys():
                        f_group = f['lalinference']['lalinference_nest']
                    else:
                        f_group = f['lalinference']['lalinference_mcmc']
                    ns = f_group['posterior_samples']
                    log_evidence = f_group.attrs['log_evidence']
                    data = array(ns[()])
                    keys = data.dtype.names
        else:
            if ext != 'dat':
                print('WARNING: data format may be incompatible...')
            data = genfromtxt(path, names=True)
            keys = data.dtype.names
            # guess location of log_evidence
            log_evidence = None
            lnz_path_guess = os.path.join(os.path.dirname(path), '*evidence*')
            lnz_paths = glob(lnz_path_guess)
            if len(lnz_paths) == 1:
                try:
                    log_evidence = np.loadtxt(lnz_paths[0])
                except Exception as e:
                    pass
        # get lambdaA posterior
        lambdaA = LambdaSamples.from_samples(data, keys=keys, alpha=alpha,
                                             cosmology=self.cosmology,
                                             log_evidence=log_evidence,
                                             label=label, path=path,
                                             **kwargs)
        self.add_result(lambdaA)

    def save_cache(self, path_template, amplitudes=False, verbose=True):
        paths = []
        for lambdaliv in self.lambdas:
            if amplitudes:
                data = lambdaliv.amplitudes
            else:
                data = lambdaliv.log_data
            path = path_template.format(label=lambdaliv.label,
                                        alpha=lambdaliv.alpha)
            np.savetxt(path, data)
            if verbose:
                print("Saved: %r" % path)
            paths.append((label, path))
        return paths

    @classmethod
    def from_cache(cls, pathdict, *args, **kwargs):
        alpha = kwargs.pop('alpha', None)
        results = cls(*args, **kwargs)
        for l, p in pdict.items():
            log_data = np.loadtxt(p)
            a = alpha if alpha is not None else guess_alpha(p)
            results.add_result_data(log_data=log_data, label=l, p=p, alpha=a)
        return results

    # -------------------------------------------------------------------------
    # STATS UTILS
    
    def reweight(self, *args, **kwargs):
        """ Reweight all posteriors following new prior.
        
        Arguments
        ---------
        prior: str
            target prior
        kwargs:
            see LambdaSamples.reweight
        """
        lambdas = [l.reweight(*args, **kwargs) for l in self.lambdas]
        return LambdaSet(lambdas=lambdas, cosmology=self.cosmology)

#     def get_combined_pdf(self, prior, nbins=256):
#         postlist = list(self.loglambdas.values())
#         # one decade wider for the KDE to fit the plot
#         x_min = min(map(min, postlist)) - 1
#         x_max = max(map(max, postlist)) + 1
#         x = linspace(x_min, x_max, nbins)
#         # KDE for each source
#         kdes = map(lambda x : stats.gaussian_kde(x, bw_method=0.02), postlist)
#         # Prior PDF on x
#         prior = prior.upper()
#         yp = jacobian[prior](x, x_min, x_max, alpha=self.alpha)
#         # evaluate the KDEs on the bins
#         # pdfs = squeeze(array([map(kde, x) for kde in kdes]), axis=2)
#         pdfs = array([kde(x) for kde in kdes])
#         # compute the combined pdf accounting for the prior
#         combined_pdf = sum(log(pdfs), axis=0) + log(yp)
#         combined_pdf = normalize(exp(combined_pdf), x)
#         return (x, combined_pdf)
#     
#     def combined_pdf(self, prior='A', **kwargs):
#         if proir not in self._combined_pdf:
#             self._combined_pdf[prior] = self.get_combined_pdf(prior, **kwargs)
#         return self._combined_pdf[prior]
# 
#     def combined_cdf(self, prior='A', **kwargs):
#         prior = prior.upper()
#         if prior not in self._combined_cdf:
#             from scipy.integrate import cumtrapz
#             x, combined_pdf = self.combined_pdf(prior=prior, **kwargs)
#             self._combined_cdf[prior] = (x, cumtrapz(combined_pdf, x, initial=0.0))
#         return self._combined_cdf[prior]
# 
#     def combined_bound(self, **kwargs):
#         cl = kwargs.pop('cl', 0.9)
#         x, cdf = self.combined_cdf(**kwargs)
#         if self.alpha < 2.0:
#             # the log is negative, so compute left-sided CL
#             p = 1 - cl
#         else:
#             # the log is positive, so compute right-sided CL
#             p = cl
#         bound = x[abs(cdf-p).argmin()]
#         return bound
#         # return pow(10.0, bound)


class LambdaPairs(object):
    def __init__(self, pos_results, neg_results, alpha, purge_mismatched=True):
        """ Container for A > 0 and A < 0 LIV results of a given alpha.

        Arguments
        ---------
        pos_results: LambdaSet
            positive-A results container.
        neg_results: LambdaSet
            negative-A results container.
        alpha: float
            dispersion index.
        """
        self.alpha = alpha
        self.results = {1: pos_results, -1: neg_results}
        # validate input: make sure we have pos and neg A values for all events
        sets = [set(r.labels) for r in self.results.values()]
        if sets[0] != sets[1]:
            mismatched = sets[0].symmetric_difference(sets[1])
            if purge_mismatched:
                print("WARNING: mismatched results for %r" % mismatched)
                for label in mismatched:
                    for res in self.results.values():
                        if label in res.labels:
                            res.remove_lambda(label)
            else:
                raise ValueError("mismatched results: %r" % mismatched)
        self._sym_amps_pdf = {}

    def __len__(self):
        return len(self.results[1])

    @property
    def labels(self):
        return self.results[1].labels

    _DEF_LABELS = {1: 'plus', -1: 'minus'}

    # -------------------------------------------------------------------------
    # METHODS
    
    @classmethod
    def from_path(cls, path_template, alpha=None, labels=None,
                  sign_labels=None, **kwargs):
        """ Loads pairs of results for multiple runs from a path template, e.g.

        > path_template = '/path/to/files/liv_{label}_A{sign}_alpha{alpha}.h5'

        which would match a path like

        > '/path/to/files/liv_S190521_Aminus_alpha3p5.h5'

        Arguments
        ---------
        path_template: str
            path template with fields for substitution.
        alpha: float
            dispersion index (opt. guessed from path)
        labels: list
            values to replace in {label} field if present (opt.)
        sign_labels: dict
            string identifiers for A signs, (def. {1: 'plus', -1: 'minus'})
        """
        alpha = alpha if alpha is not None else guess_alpha(path_template)
        alpha_str = default_alpha_string(alpha)
        sign_labels = sign_labels or cls._DEF_LABELS
        if path_template.replace('{label}', '') == path_template:
            labels = ['0']
        results = {}
        for s, s_label in sign_labels.items():
            paths = [path_template.format(label=l, sign=s_label,
                                          alpha=alpha_str) for l in labels]
            results[s] = LambdaSet.from_paths(paths, labels=labels, **kwargs)
        return cls(results[1], results[-1], alpha=alpha)

    # -------------------------------------------------------------------------
    # STATS UTILS

    def reweight(self, *args, **kwargs):
        """ Reweight all posteriors following new prior.
        
        See LambdaSet.reweight()
        """
        res = {s: r.reweight(*args, **kwargs) for s,r in self.results.items()}
        return LambdaPairs(res[1], res[-1], alpha=self.alpha)

    def compute_twosided_amplitude_pdf(self, label, nbins=1000, reweight=None,
                                        **kwargs):
        """ Join posterior for positive and negative A's, weighting by evidence.

        Arguments
        ---------
        label: str, int
            label or index of run to combine
        nbins: int
            number of points to interpolate KDE (def. 1000)
        reweight: str
            reweight samples by amplitude ('A') or mass ('MASS')
            Jacobian (def. None, i.e. no reweighting)
        """
        # get amplitude samples and evidences
        amps, logz = {}, {}
        for s, r in self.results.items():
            lambdaliv = r.get_lambda(label)
            amps[s] = s*lambdaliv.amplitudes
            logz[s] = lambdaliv.log_evidence or 0
        # pick same number of positive and negative samples
        # (without assuming samples are randomly sorted)
        ns = min([len(a) for a in amps.values()])
        amps_ns = {s: np.random.choice(a, size=ns, replace=False)
                   for s,a in amps.items()}
        amps_array = np.append(amps_ns[-1], amps_ns[1])
        # construct weights for positive and negative samples
        neg_weights = 1./(np.exp(logz[1] - logz[-1]) + 1.)
        weights = np.append(np.ones(ns)*neg_weights,
                            np.ones(ns)*(1. - neg_weights))
        # construct Jacobian weights
        if reweight:
            if reweight.upper() == 'A':
                print('Reweight: A')
                weights *= np.abs(amps_array) / np.sum(np.abs(amps_array))
        weights /= np.sum(weights)
        # create PDF
        pdf = stats.gaussian_kde(amps_array, weights=weights, **kwargs)
        # could cal the KDE directly as below, but interp1d is *much* faster!
        # TODO: is there a better way to do this?
        x = linspace(2*min(amps_array), 2*max(amps_array), nbins)
        return interp1d(x, pdf(x), fill_value=0, bounds_error=False)

    def get_twosided_amplitude_pdf(self, label, **kwargs):
        # # TODO: do we ever need to cache the KDEs? disabling for now
        #
        # if label not in self._sym_amps_pdf:
        #     self._sym_amps_pdf[label] = self.compute_twosided_amplitude_pdf(label, **kwargs)
        # return self._sym_amps_pdf[label]
        return self.compute_twosided_amplitude_pdf(label, **kwargs)

    def draw_twosided_amplitudes(self, label, nsamp=1000, kde_kws=None, **kwargs):
        """ Draw samples from PDF produced jointly from A>0 and A<0 samples.

        Arguments
        ---------
        label: str, int
            label or index of run
        nsamp: int
            number of samples to draw

        Returns
        -------
        samples: array
        """
        if kde_kws is None:
            kde_kws = {}
        pdf = self.get_twosided_amplitude_pdf(label, **kde_kws)
        x_min = -max(self.results[-1].get_lambda(label).amplitudes)
        x_max = max(self.results[1].get_lambda(label).amplitudes)
        return draw_samples(pdf, x_min, x_max, nsamp, **kwargs)
        
    def draw_all_twosided_amplitudes(self, **kws):
        return [self.draw_twosided_amplitudes(l, **kws) for l in self.labels]


    def get_all_twosided_amplitude_pdfs(self, **kws):
        output = []
        # make it so this can be called wit the same arguments as
        # `draw_all_twosided_amplitudes`, for convenience
        kde_kws = kws.pop('kde_kws', {})
        kws.update(kde_kws)
        for label in self.labels:
            pdf = self.get_twosided_amplitude_pdf(label, **kws)
            x_min = -max(self.results[-1].get_lambda(label).amplitudes)
            x_max = max(self.results[1].get_lambda(label).amplitudes)
            x = np.linspace(x_min, x_max, kws.get('nbins', 1000))
            output.append([x, pdf(x)])
        return output
